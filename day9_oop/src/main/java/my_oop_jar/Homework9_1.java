package my_oop_jar;

public class Homework9_1 {
    public static void main(String[] args) {
        CEO marvel = new CEO("Captain", "Marvel", 30000);
        Employee somsri = new Employee("Somsri","Sudsuay",22000);
        somsri.gossip(marvel,"Today is very cold!");
        marvel.gossip(somsri,"Today is very cold!");
        marvel.work(somsri);

        marvel.assignNewSalary(somsri, 20);
        marvel.assignNewSalary(somsri, 25000);

    }
}
