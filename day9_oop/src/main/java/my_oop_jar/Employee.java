package my_oop_jar;

public class Employee {
    public   String firstname;
    public   String lastname;
    private  int salary;
    public static String planet = "Earth";
    public static int freeChocolateLeft = 20;
   
    public Employee(String firstnameInput, String lastnameInput, int salaryInput) {
        this.firstname = firstnameInput;
        this.lastname = lastnameInput;
        this.salary = salaryInput;
    }
    public void hello() {
        System.out.println("Hello " + this.firstname );
    }
    public int getSalary() {
        return salary;
    }
    public void gossip(Employee emp,String word){
        System.out.println("Hey "+emp.firstname+","+word);
    }
}

