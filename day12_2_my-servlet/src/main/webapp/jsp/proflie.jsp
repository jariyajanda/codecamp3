<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>

<head lang="en">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Homework</title>

    <link href="<c:url value="/css/style.css" />" rel="stylesheet">
    <script src="<c:url value="/js/app.js" />"></script>
</head>

<body>
    <div class="container">
        <br>
        <!-- Nav pills -->
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="home">About Me</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="skill">Skill </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="contact">Contact Me</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="proflie">Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="resigter">resigter</a>
            </li>
        </ul>
        <div class="row">
            <div class="col-sm-3">
                <img src="/image/47138.jpg">
            </div>
            <div class="col-sm-3" style="background-color:lavenderblush;">
                <ul class="portfo">
                    <li class="portfo">เชียงใหม่</li>
                    <li class="portfo">เชียงดาว</li>
                    <li class="portfo">บ้านต้นไม้แม่แมะ</li>
                </ul>
            </div>
            <div class="col-sm-3">
                <img src="/image/47139.jpg">
            </div>
            <div class="col-sm-3" style="background-color:lavenderblush;">
                <ul class="portfo">
                    <li class="portfo">กรุงเทพ</li>
                    <li class="portfo">เอกมัย</li>
                    <li class="portfo">เรียน Ad Facebook</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <img src="/image/47140.jpg">
            </div>
            <div class="col-sm-3" style="background-color:lavenderblush;">
                <ul class="portfo">
                    <li class="portfo">เขาใหญ่</li>
                    <li class="portfo">สวนดอกไม้</li>
                </ul>
            </div>
            <div class="col-sm-3">
                <img src="/image/47142.jpg">
            </div>
            <div class="col-sm-3" style="background-color:lavenderblush;">
                <ul class="portfo">
                    <li class="portfo">ภูเก็ต</li>
                    <li class="portfo">เมืองเก่า</li>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>