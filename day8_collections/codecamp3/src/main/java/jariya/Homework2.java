package jariya;

import java.util.Map.Entry;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

class Homework2 {
  public static void main(String[] args) {
    String[] rawData = { "id:1001 firstname:Luke lastname:Skywalker", "id:1002 firstname:Tony lastname:Stark",
        "id:1003 firstname:Somchai lastname:Jaidee", "id:1004 firstname:MonkeyD lastname:Luffee" };

    for (String item : rawData) {
      String[] splitString = item.split(" ");
      ArrayList<String> mySplitItem = new ArrayList<>(Arrays.asList(splitString));
      System.out.println(mySplitItem);
      HashMap<String, String> strHashMap = new HashMap<>();
      for (String has : mySplitItem) {
        String[] splitHasmap = has.split(":");
        String keystr = splitHasmap[0];
        String valuestr = splitHasmap[1];
        strHashMap.put(keystr, valuestr);
      }
      for (String key : strHashMap.keySet()) {
        System.out.println(key + ": " + strHashMap.get(key));
      }

    }

  }
}

/*
 * ArrayList<String> mySplitItem = new ArrayList<String>();
 * ArrayList<HashMap<String, String>> myhashmap = new ArrayList<HashMap<String,
 * String>>();
 * 
 * for( String element:rawData) { String[] splitString = element.split(" ");
 * mySplitItem.addAll(new ArrayList<>(Arrays.asList(splitString))); int size =
 * splitString.length; for (int i = 0; i < size; i++) { String item =
 * splitString[i]; String[] spliteKey = item.split(":"); int sizeKey =
 * spliteKey.length; HashMap<String, String> itemHashMap = new HashMap<String,
 * String>(); if (sizeKey == 2) { String keyItem = spliteKey[0]; String
 * valueItem = spliteKey[1]; itemHashMap.put(keyItem, valueItem); }
 * myhashmap.add(itemHashMap); // System.out.println(myhashmap); } }
 * 
 * Iterator<HashMap<String, String>> mySplitItemListIterator = myhashmap
 * .iterator();while(mySplitItemListIterator.hasNext()) { HashMap<String,
 * String> itemIterator = mySplitItemListIterator.next();
 * 
 * Iterator<Entry<String, String>> entrySet =
 * itemIterator.entrySet().iterator(); while (entrySet.hasNext()) {
 * Entry<String, String> key = entrySet.next(); System.out.println(key.getKey()
 * + ":" + key.getValue()); }
 * 
 * }
 */