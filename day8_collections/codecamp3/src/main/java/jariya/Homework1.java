package jariya;

import java.util.HashMap;
import java.util.ArrayList;

class Homework1 {
  public static void main(String[] args) {
    String[] rawData = { "id:1001 firstname:Luke lastname:Skywalker", "id:1002 firstname:Tony lastname:Stark",
        "id:1003 firstname:Somchai lastname:Jaidee", "id:1004 firstname:MonkeyD lastname:Luffee" };
    ArrayList<HashMap<String, String>> mySplitItem = new ArrayList<>();
    for (String element : rawData) {
      String[] splitString = element.split(" ");
      HashMap<String, String> myMap = new HashMap<String, String>();
      int size = splitString.length;
      for (int i = 0; i < size; i++) {
        String item = splitString[i];
        String[] spliteKey = item.split(":");
        // int sizeKey = spliteKey.length;
        String keyItem = spliteKey[0];
        String valueItem = spliteKey[1];
        myMap.put(keyItem, valueItem);

      }
      mySplitItem.add(myMap);

    }

    for (HashMap<String, String> myMap : mySplitItem) {
      for (String id : myMap.keySet()) {
        System.out.println(id + ":" + myMap.get(id));
      }

    }
  }
}

/*
 * for (int i = 0; i < mySplitItem.size(); i++) { //
 * myMap.put(mySplitItem.get(i)); System.out.println(mySplitItem.get(i));
 * 
 * } /* for (int i = 0; i < mySplitItem.size(); i++) {
 * System.out.println(mySplitItem.get(i)); } for (String key :
 * mySplitItem.keySet()) { System.out.println(key+ ": "+ mySplitItem.get(key));
 * }
 */
