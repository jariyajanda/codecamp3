package cc.rest.Service;

import java.math.BigDecimal;

import cc.rest.Entity.BankAccount;
import cc.rest.Entity.Person;

public class BankService {

    public static Person openAccount(String accountNo, String personId, String personFullName,
            BigDecimal openingBalance) {
        return null;
    }

    public static void deposit(BankAccount bankAccount, BigDecimal amount) {
        BankAccount ba = BankDao.findBankAccountByNo(bankAccount);
        // BigDecimal balance = ba.getBalance() == null ? new BigDecimal("0") :
        // ba.getBalance();
        BigDecimal balance = ba.getBalance().add(amount);
        ba.setBalance(balance);
        BankDao.updateBankAccount(ba);
    }

    public static void withdow(BankAccount bankAccount, BigDecimal amount) {
        BankAccount ba = BankDao.findBankAccountByNo(bankAccount);
        BigDecimal balance = ba.getBalance().subtract(amount);
        ba.setBalance(balance);
        BankDao.updateBankAccount(ba);
    }

    public static void closeAccount(BankAccount bankAccount) {
        bankAccount.setStatus(0);
        BankDao.updateBankAccount(bankAccount);
    }

    // public static void deposit(BankAccount bankAccount, BigDecimal amount) {

    // BankAccount bankaccout = BankDao.findRecordById(ba.getAccountNo());
    // // BigDecimal oldamount = bankaccout.getBalance() == null ? new
    // BigDecimal("0") : bankaccout.getBalance();
    // BigDecimal newamount = oldamount.add(amount);
    // bankaccout.setBalance(newamount);
    // BankDao.updateRecord(bankaccout);
    // }

    // public static void withdow(BankAccount ba, BigDecimal amount) {

    // BankAccount bankaccout = BankDao.findRecordById(ba.getAccountNo());
    // BigDecimal oldamount = bankaccout.getBalance();
    // BigDecimal newamount = oldamount.subtract(amount);
    // bankaccout.setBalance(newamount);
    // BankDao.updateRecord(bankaccout);
    // }
}