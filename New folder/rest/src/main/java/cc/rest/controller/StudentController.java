package cc.rest.controller;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cc.rest.Entity.Person;
import cc.rest.Service.BankService;

@RestController
public class StudentController {
    @GetMapping("/camp3/api/{student}")
    public ResponseEntity<String> getGrade(@PathVariable("student") String student,
            @RequestParam("grade") String grade) {
        return new ResponseEntity<String>(student + " has got grade F", HttpStatus.OK);
    }

    @GetMapping("/camp3/api/{accountNo}/{personId}/{personFullName}/{openingBalance}")
    public ResponseEntity<String> openAccount(@PathVariable("accountNo") String accountNo,
            @PathVariable("personId") String personId, @PathVariable("personFullName") String personFullName,
            @PathVariable("openingBalance") String openingBalance) {
        Person person = BankService.openAccount(accountNo, personId, personFullName,
                BigDecimal.valueOf(Double.parseDouble(openingBalance)));
        return new ResponseEntity<String>(person.toString(), HttpStatus.OK);
    }
}
