package demo.openaccount.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import demo.openaccount.entity.AppRole;
import demo.openaccount.entity.AppUser;
import demo.openaccount.entity.UserRole;
import demo.openaccount.repository.AppUserRepository;
import demo.openaccount.repository.UserRoleRepository;
import demo.openaccount.util.PasswordUtil;

@Service
public class AppUserService {
    @Autowired
    AppRoleRepository appRoleRepository;
    @Autowired
    AppUserRepository appUserRepository;
    @Autowired
    UserRoleRepository userRoleRepository;

    public String registerAndValidation(String username, String password, String confirmPassword, String firstname,
            String lastname, String email, String confirmEmail, String birthDay, String birthMonth, String birthYear,
            String gender) {
        String errorMessage = "";

        if (!password.equals(confirmPassword))
            errorMessage += "Password mismatched!<br>";

        if (!email.equals(confirmEmail))
            errorMessage += "Email mismatched!<br>";

        String birthDayDatabaseFormat = birthYear + "-" + birthMonth + "-" + birthDay;
        if (errorMessage.equals("")) { // no error
            try {
                String encryptedPassword = PasswordUtil.encryptPassword(password);
                createUser(username, encryptedPassword, firstname, lastname, email, birthDayDatabaseFormat, gender);
            } catch (Exception ex) {
                errorMessage += ex.getMessage();
            }
        }

        return errorMessage;
    }

    public void createUser(String username, String encryptedPassword, String firstname, String lastname, String email,
            String birthDate, String gender) {
        AppUser user = new AppUser(username, encryptedPassword, firstname, lastname, email, birthDate, gender);
        AppRole role = appRoleRepository.findByRoleName("ROLE_USER");
        UserRole userRole = new UserRole(user, role);

        appUserRepository.save(user);
        userRoleRepository.save(userRole);
    }

    public AppUser findByUserName(String userName) {
        AppUser appUser = appUserRepository.findByUserName(userName);
        return appUser;
    }

    public Iterable<AppUser> findAll() {
        return appUserRepository.findAll();
    }

    public AppUser findById(Long userId) {
        AppUser appUser = appUserRepository.findById(userId).get();
        return appUser;
    }

    public Collection<AppUser> findByGender(String gender) {
        Collection<AppUser> appUser = appUserRepository.findByGender(gender);
        for(AppUser app : appUser){
            System.out.println(app.getFirstName());
        }
        return appUser;
    }
    
    public Collection<AppUser> findByEmail(String email) {
        Collection<AppUser> appUser = appUserRepository.findByEmail(email);
        for(AppUser app : appUser){
            System.out.println(app.getEmail());
        }
        return appUser;
    }

    public AppUser findByFullName(String firstname,String lastname) {
        AppUser appUser = appUserRepository.findByFullName(firstname,lastname);

        return appUser;
    }
}