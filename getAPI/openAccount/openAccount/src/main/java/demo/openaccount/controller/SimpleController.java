package demo.openaccount.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import demo.openaccount.domain.Data;
import demo.openaccount.service.DataService;

@Controller
public class SimpleController {

    @Autowired
    DataService dataService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public String hello(ModelMap modelMap) {
        modelMap.addAttribute("message", "Welcome to Spring Web");
        return "hello";
    }

    @RequestMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public Object getAllEmployeesJSON(Model model) {
        String json = "https://jsonplaceholder.typicode.com/todos/1";
        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.getForObject(json, Object.class);
        System.out.println(result);
        model.addAttribute("result", result);

        return result;
    }

    // @PostMapping(path = "/camp3/api/date", produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // public ResponseEntity<Date> getAll(@RequestBody Response response) {
    // RestTemplate restTemplate = new RestTemplate();
    // Date date = new Date();
    // // restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
    // // System.out.println(result);
    // System.out.println("JSON");
    // return new ResponseEntity<Date>(date, HttpStatus.OK);
    // }

    @PostMapping(path = "/camp3/api/date", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Data> getJson(HttpServletRequest request) {

        try {
            request.setCharacterEncoding("utf-8");
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            //Car[] cars = objectMapper.readValue(jsonCarArray, Car[].class);
            BufferedReader str = request.getReader();
            String strjson = "";
            String strline = "";
            while (true) {
                strline = str.readLine();
                if (strline == null) {
                    break;
                }
                strjson += strline;
            }
            System.out.println(strjson);
            List<Data> myObjects = objectMapper.readValue(strjson, new TypeReference<List<Data>>() {
            });

            dataService.insertData(myObjects);
            System.out.println(myObjects);
            return new ResponseEntity<Data>(new Data(), HttpStatus.valueOf(200));

        } catch (IOException ex) {
            System.out.println("JSON parse error: " + ex.getMessage());
        }
        return null;

    }

    @RequestMapping(value = "/emp", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public String openAccount() {
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            ArrayList<HashMap<String, String>> employees = objectMapper.readValue(
                    new URL("https://jsonplaceholder.typicode.com/posts"),
                    new TypeReference<ArrayList<HashMap<String, String>>>() {
                    });

            for (int i = 0; i < employees.size(); i++) {
                System.out.println("userId: " + employees.get(i).get("userId"));
                System.out.println("id: " + employees.get(i).get("id"));
                System.out.println("title: " + employees.get(i).get("title"));
                System.out.println("body: " + employees.get(i).get("body"));

            }
        } catch (IOException ex) {
            System.out.println("JSON parse error: " + ex.getMessage());
        }
        return null;
    }
}
