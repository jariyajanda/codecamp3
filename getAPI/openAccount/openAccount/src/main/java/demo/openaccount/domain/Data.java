package demo.openaccount.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Data {
    private Double x;
    private Double y;
    private Double z;
    private String date;
    private boolean isRed;

    public Data() {

    }

    public Data(Double x, Double y, Double z, String date, boolean isRed) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.date = date;
        this.isRed = isRed;
    }

    @JsonGetter("x")
    public Double getX() {
        return x;
    }

    @JsonSetter("x")
    public void setX(Double x) {
        this.x = x;
    }

    @JsonGetter("y")
    public Double getY() {
        return y;
    }

    @JsonSetter("y")
    public void setY(Double y) {
        this.y = y;
    }

    @JsonGetter("z")
    public Double getZ() {
        return z;
    }

    @JsonSetter("z")
    public void setZ(Double z) {
        this.z = z;
    }

    @JsonGetter("Date")
    public String getDate() {
        return date;
    }

    @JsonSetter("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonGetter("isred")
    public boolean isRed() {
        return isRed;
    }

    @JsonSetter("isred")
    public void setRed(boolean isRed) {
        this.isRed = isRed;
    }

}