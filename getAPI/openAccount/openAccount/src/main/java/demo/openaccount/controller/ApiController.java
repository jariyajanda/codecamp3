package demo.openaccount.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import demo.openaccount.entity.AppUser;
import demo.openaccount.service.AppUserService;

@RestController
public class ApiController {
    @Autowired
    private AppUserService appUserService;
    
    @RequestMapping("/HelloWorld")
    @ResponseBody
    String home() {
        return "Hello World!";
    }
    @RequestMapping("/api/getUser/{username}")
    @ResponseBody
    AppUser getUser(@PathVariable("username") String username) {
        AppUser userInfo = appUserService.findByUserName(username);
        return userInfo;
    }
    @RequestMapping("/api/getUserById/{user_id}")
    @ResponseBody
    AppUser getUserById(@PathVariable("user_id") Long userId) {
        try{
            AppUser userInfo = appUserService.findById(userId);
            return userInfo;
        }catch(java.util.NoSuchElementException exception){
            return null;
        }
    }
    @RequestMapping(value = "/api/getGender/{gender}", method = RequestMethod.GET)
    @ResponseBody
    Collection<AppUser> getGender(@PathVariable("gender") String gender) {
        Collection<AppUser> userInfo = appUserService.findByGender(gender);
            return userInfo;
    }
    @RequestMapping(value = "/api/getGender/{email}", method = RequestMethod.GET)
    @ResponseBody
    Collection<AppUser> getEmil(@PathVariable("email") String email) {
        Collection<AppUser> userInfo = appUserService.findByGender(email);
            return userInfo;
    }
    @RequestMapping(value = "/api/getFullName/{firstname}/{lastname}", method = RequestMethod.GET)
    @ResponseBody
    AppUser getFullName(@PathVariable("firstname") String firstname,@PathVariable("lastname") String lastname) {
        AppUser userAll = appUserService.findByFullName(firstname, lastname);
        return userAll;
    }
    @RequestMapping(value = "/api/getUserAll", method = RequestMethod.POST)
    @ResponseBody
    Iterable<AppUser> getUserAll() {
        Iterable<AppUser> userAll = appUserService.findAll();
        return userAll;
    }

}
