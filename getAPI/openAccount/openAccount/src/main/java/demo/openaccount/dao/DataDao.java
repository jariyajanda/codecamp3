package demo.openaccount.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import demo.openaccount.domain.Data;
import demo.openaccount.entity.DataEntity;
import demo.openaccount.repository.DataRepository;

@Component
public class DataDao {

    @Autowired
    private DataRepository dataRepository;

    public final static Logger logger = LogManager.getLogger(DataDao.class);

    public DataEntity insertData(Data item) {
        System.out.println(item.getDate());
      
        DataEntity itementity = new DataEntity(item.getX(), item.getY(), item.getZ(), item.getDate(), item.isRed());
        System.out.println(itementity.getX());
        dataRepository.save(itementity);

        return itementity;
    }

}
