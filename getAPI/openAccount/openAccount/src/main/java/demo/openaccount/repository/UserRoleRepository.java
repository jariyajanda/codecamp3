package demo.openaccount.repository;

import org.springframework.data.repository.CrudRepository;

import demo.openaccount.entity.UserRole;

public interface UserRoleRepository extends CrudRepository<UserRole, Long>  {

}