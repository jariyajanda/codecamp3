package demo.openaccount.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "datasource")
public class DataEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;
    @Column(name = "x")
    private Double x;
    @Column(name = "y")
    private Double y;
    @Column(name = "z")
    private Double z;
    @Column(name = "date")
    private String date;
    @Column(name = "isRed")
    private boolean isRed;

    public DataEntity() {

    }

    public DataEntity(Double x, Double y, Double z, String date, boolean isRed) {
        long timestemp = 0;
        // try {
        //     timestemp = new SimpleDateFormat("dd/MM/yyyy").parse(date).getTime();
        // } catch (ParameterMisuseException e) {
        //     e.printStackTrace();
        // }
        this.x = x;
        this.y = y;
        this.z = z;
        //this.date = new Date(timestemp);
        this.date = date;
        this.isRed = isRed;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Double getZ() {
        return z;
    }

    public void setZ(Double z) {
        this.z = z;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isRed() {
        return isRed;
    }

    public void setRed(boolean isRed) {
        this.isRed = isRed;
    }
}