package demo.openaccount.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import demo.openaccount.entity.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, Long> {
    public AppUser findByUserName(String userName);
    @Query(value = "SELECT * FROM app_user WHERE gender = ?1",
    nativeQuery = true)
    public Collection<AppUser> findByGender(String gender);

    @Query(value = "SELECT * FROM app_user WHERE email = ?1",
    nativeQuery = true)
    public Collection<AppUser> findByEmail(String email);

    @Query(value = "SELECT * FROM app_user WHERE firstname = ?1 AND lastname =?2",
    nativeQuery = true)
    public AppUser findByFullName(String firstname,String lastname);
}