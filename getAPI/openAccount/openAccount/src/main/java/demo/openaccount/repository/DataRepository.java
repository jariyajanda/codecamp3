package demo.openaccount.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import demo.openaccount.entity.DataEntity;

@Repository
public interface DataRepository extends CrudRepository<DataEntity, Integer> {
}