package demo.openaccount.service;

import org.springframework.data.repository.CrudRepository;

import demo.openaccount.entity.AppRole;

public interface AppRoleRepository extends CrudRepository<AppRole, Long>  {
    AppRole findByRoleName(String roleName);
}