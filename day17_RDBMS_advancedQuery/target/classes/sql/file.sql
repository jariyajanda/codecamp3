select students.name as studentName, max(price) as maxPrice
from courses
    inner join enrolls on enrolls.course_id = courses.id
    inner join students on students.id = enrolls.student_id
group by students.name


select *
from courses c
    JOIN (
        select enrolls.student_id, students.name, max(courses.price) as maxprice
    from courses
        inner join enrolls on enrolls.course_id = courses.id
        inner join students on enrolls.student_id = students.id
    group by enrolls.student_id) as t2
    on c.price = t2.maxprice;



select enrolls.student_id, students.name, avg(courses.price)
from courses
    inner join enrolls on enrolls.course_id = courses.id
    inner join students on enrolls.student_id = students.id
group by enrolls.student_id;


select students.name as studentName, count(price), sum(price) as sumPrice
from courses
    inner join enrolls on enrolls.course_id = courses.id
    inner join students on students.id = enrolls.student_id
group by students.name