<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
</head>

<body>
    <table border="1">
        <tr>

            <th>Courses Name</th>
            <th>Sum price</th>

        </tr>
        <c:forEach var="item" items="${cours}">
            <tr>
                <td>
                    <c:out value="${item.getCousName()}" />
                </td>
                <td>
                    <c:out value="${item.getPrice()}" />
                </td>
            </tr>
        </c:forEach>
    </table>
</body>

</html>