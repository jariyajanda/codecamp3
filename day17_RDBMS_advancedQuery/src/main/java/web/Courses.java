package web;

public class Courses {
    private int cousId;
    private String cousName;
    private int price;

    /**
     * @return the cousId
     */
    public int getCousId() {
        return cousId;
    }

    /**
     * @param cousId the cousId to set
     */
    public void setCousId(int cousId) {
        this.cousId = cousId;
    }

    /**
     * @return the cousName
     */
    public String getCousName() {
        return cousName;
    }

    /**
     * @param cousName the cousName to set
     */
    public void setCousName(String cousName) {
        this.cousName = cousName;
    }

    /**
     * @return the piec
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param piec the piec to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

}