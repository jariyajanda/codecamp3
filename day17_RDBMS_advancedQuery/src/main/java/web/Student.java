package web;

public class Student {
    private int id;
    private String StudentName;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return StudentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

}