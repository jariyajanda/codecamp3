package web;

public class Enrolls {
    private int istudent_d;
    private int course_id;

    /**
     * @return the istudent_d
     */
    public int getIstudent_d() {
        return istudent_d;
    }

    /**
     * @param istudent_d the istudent_d to set
     */
    public void setIstudent_d(int istudent_d) {
        this.istudent_d = istudent_d;
    }

    /**
     * @return the course_id
     */
    public int getCourse_id() {
        return course_id;
    }

    /**
     * @param course_id the course_id to set
     */
    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

}