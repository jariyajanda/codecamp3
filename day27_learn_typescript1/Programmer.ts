import { Employee } from './Employee';
//import { ICleaner } from './my_interface/ICleaner';
import { ICoachroachKiller } from './my_interface/ICoachroachKiller';
export class Programmer extends Employee implements IWebsiteCreator, IWindowsInstaller {
    firstname: String;
    lastname: String;
    salary: number;
    constructor(firstname: string, lastname: string, salary: number) {
        super(firstname, lastname, salary);
        // this.firstname = firstname;
        // this.lastname = lastname;
        // this.salary = salary;
    }

    fixPC(serialNumber: String) {
        console.log(`I'm trying to fix PC serialNumber: ${serialNumber}`);
    }
    createWebsite(template: String, titleName: String) {
        console.log(`Create website by Programmer : ${titleName} ${template}`);
    }
    installWindows(version: String, productKey: String) {
        console.log(`version: ${version} product: ${productKey}`);
    }

    formatWindows(drive: String) {
        console.log(`Format windows : ${drive}`);
    }
    LastInstalledWindowsVersion(): number {
        return 10;
    }
}
