export interface Building {
    buildingName: string;
    roomName: string;
}