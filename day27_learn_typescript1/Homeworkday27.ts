import { Employee } from './Employee';
import { Programmer } from './Programmer';
import { OfficeCleaner } from './OfficeCleaner';
import { CEO } from './CEO';


const captain = new CEO('Captain', 'Marvel', 30000);
console.log(captain.firstname);
captain.hello();
console.log(captain.installWindows('10', 'key'));
const bob = new Programmer("Bob", "Red", 20000);
console.log(bob.firstname);
const somsi = new OfficeCleaner('somsi', 'medee', 9000);
console.log(somsi.firstname);
let cleanedRoom = JSON.stringify(somsi.getCleanedRoom())
console.log(`Somsi cleaned at ${cleanedRoom}`);