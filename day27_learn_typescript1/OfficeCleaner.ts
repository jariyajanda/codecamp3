import { Employee } from './Employee';
import { ICleaner } from './my_interface/ICleaner';
import { ICoachroachKiller } from './my_interface/ICoachroachKiller';
import { Building } from './building';

export class OfficeCleaner extends Employee implements ICleaner, ICoachroachKiller {
    //resultByHuman = Building;
    //resultByAI: Building;
    buildingObj = {} as Building;
    totalKilled: number;

    DecorateRoom() {
        console.log("DecorateRoom");
    }
    WelcomeGuest() {
        console.log("WelcomeGuest");
    }

    setTools(toolName: String) {
        console.log(`setTools Method ${toolName}`);
    }

    clean(building: String, roomName: String) {
        // Array < String, String > hash = new Array<>();
        // hash.push("building", building);
        // hash.push("roomName", roomName);
        // resultByHuman.add(hash);
        //System.out.println("clear Method building"+ building+ "roomName" + roomName);
        //System.out.println("building :"+building+"roomName"+roomName);
    }

    getCleanedRoom(): Building {
        this.buildingObj = { buildingName: 'Software Park', roomName: 'room 301' };
        return this.buildingObj;
    }

    buyBaygon(numBaygon: number) {
        console.log(`Baygon : ${numBaygon}`);
    }

    killCoachroach(baygonNumber: number, building: String, roomName: String) {
        console.log(`killCoachroach ${baygonNumber} ${building} ${roomName}`);
    }

    getTotalKilled(): number {
        this.totalKilled += 100;
        this.killCoachroach(100, " building ", " roomName ");
        return this.totalKilled;
    }
    CleanedRoom() {
        console.log("method clean room");
    }

}