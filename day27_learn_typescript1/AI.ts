import { Building } from './building';
import { ICleaner } from './my_interface/ICleaner';
import { ICoachroachKiller } from './my_interface/ICoachroachKiller';
class AI implements IWebsiteCreator, ICleaner, ICoachroachKiller {
    name: string;
    language: String;
    totalKilled: number;
    resultByAI: Building;
    buildingObj = {} as Building;

    constructor(nameInput: string, languageInput: string) {
        this.name = nameInput;
        this.language = languageInput;

    }
    createWebsite(template: string, language: string) {
        console.log(`${language} automated Setup template: ${template}`);
        console.log(`${language} automated Set Title name: ${template}`);
    }

    setTools(toolName: string) {
        console.log("Methpd set tools");
    }

    clean(building: string, roomName: string) {
        // HashMap < String, String > hashAI = new HashMap<>();

        // hashAI = new Array<string>();
        console.log();
        // hashAI.push(building);
        // hashAI.push(roomName);
        // resultByAI.push(hashAI);
    }

    buyBaygon(numBaygon: number) {
        console.log(`Baygon number: ${numBaygon}`);
    }

    killCoachroach(baygonNumber: number, building: String, roomName: String) {
        console.log(`${baygonNumber} building  ${roomName}`);
    }

    getTotalKilled() {
        this.totalKilled += 100;
        return this.totalKilled;
    }
    getCleanedRoom(): Building {
        this.resultByAI = { buildingName: 'Software Park', roomName: 'room 301' };
        return this.resultByAI;
    }


}
