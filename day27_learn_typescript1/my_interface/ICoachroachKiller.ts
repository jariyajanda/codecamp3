export interface ICoachroachKiller {
    buyBaygon(numBaygon: number);
    killCoachroach(baygonNumber: number, building: String, roomName: String);
    getTotalKilled();
}