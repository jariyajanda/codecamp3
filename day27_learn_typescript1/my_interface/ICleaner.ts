import { Building } from "../building";

export interface ICleaner {
    setTools(toolName: string);
    clean(building: string, roomName: string);
    getCleanedRoom(): Building;
}