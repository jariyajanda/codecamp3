interface IWindowsInstaller {
    formatWindows(drive: String);
    installWindows(version: String, productKey: String);
    LastInstalledWindowsVersion();
}