interface IWebsiteCreator {
    createWebsite(template: String, titleName: String);
}
