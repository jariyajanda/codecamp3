export class Employee {

    firstname: String;
    lastname: String;
    salary: number;

    constructor(firstname: String, lastname: String, salary: number) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    hello() {
        console.log("Employee hello");
    }

}