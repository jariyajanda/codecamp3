import { Employee } from './Employee';
import { ICleaner } from './my_interface/ICleaner';
import { ICoachroachKiller } from './my_interface/ICoachroachKiller';
export class CEO extends Employee {

    constructor(firstname: String, lastname: String, salary: number) {
        super(firstname, lastname, salary);
        // this.firstname = firstname;
        // this.lastname = lastname;
        // this.salary = salary;
    }

    hello() {
        console.log(`Hi, nice to meet you. ${this.firstname} !`);
    }

    orderWebsite(creator: IWebsiteCreator) {
        creator.createWebsite("some template", "Codecamp3");
    }

    installWindows(version: String, productKey: String): string {
        return `versin is ${version} prodoct key is ${productKey}`;
    }

    orderKillCoachroach(clear: ICoachroachKiller) {
        clear.killCoachroach(100, "building", "roomName");
    }

    orderCleaned(cleaner: ICleaner, building: string, roomName: string) {
        cleaner.clean(building, roomName);
    }
}
