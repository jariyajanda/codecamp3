let text: string;
text = 'Hello World';
console.log(text);

let num: number = 25;
let studentName: string = 'Codecamp';
let arr: string[] = ['a', 'b'];
const x: number = 20;
const y: number = 25;
console.log(num);
console.log(studentName);
console.log(arr);
console.log(x);
console.log(y);
function calculator(x: number, y: number): number {
    return x + y;
}
console.log(calculator(x, y));

let items: number[];
items = [];
items.push(1);
items.push(2);
console.log(items);

class Animal {
    coler: string;

}
class Bird extends Animal implements Flyable {
    name: string;
    constructor(name: string) {
        super();
        this.name = name;
    }
    myName() {
        console.log(` My name is ${this.name} `);
    }
    fly() {
        console.log('I’m flying');
    }
}

interface Flyable {
    fly();
}

let AA: Bird;
AA = new Bird("bird one");
AA.name = "jjjjj";
AA.myName();
AA.coler = "red";
AA.fly();
let BB: Bird;
BB = new Bird("bird two");
BB.fly();

import { Calculator, student } from './student-list';
const cal = new Calculator();
cal.calculate(1, 2);

for (let items of student) {
    console.log('student ' + items);
}
