"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Programmer_1 = require("./Programmer");
var OfficeCleaner_1 = require("./OfficeCleaner");
var CEO_1 = require("./CEO");
var captain = new CEO_1.CEO('Captain', 'Marvel', 30000);
console.log(captain.firstname);
captain.hello();
console.log(captain.installWindows('10', 'key'));
captain.installWindows('10', 'key');
var bob = new Programmer_1.Programmer("Bob", "Red", 20000);
console.log(bob.firstname);
var somsi = new OfficeCleaner_1.OfficeCleaner('somsi', 'medee', 9000);
console.log(somsi.firstname);
var cleanedRoom = JSON.stringify(somsi.getCleanedRoom());
console.log("Somsi cleaned at " + cleanedRoom);
//# sourceMappingURL=Homeworkday27.js.map