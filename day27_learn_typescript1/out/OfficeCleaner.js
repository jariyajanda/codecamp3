"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Employee_1 = require("./Employee");
var OfficeCleaner = /** @class */ (function (_super) {
    __extends(OfficeCleaner, _super);
    function OfficeCleaner() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //resultByHuman = Building;
        //resultByAI: Building;
        _this.buildingObj = {};
        return _this;
    }
    OfficeCleaner.prototype.DecorateRoom = function () {
        console.log("DecorateRoom");
    };
    OfficeCleaner.prototype.WelcomeGuest = function () {
        console.log("WelcomeGuest");
    };
    OfficeCleaner.prototype.setTools = function (toolName) {
        console.log("setTools Method " + toolName);
    };
    OfficeCleaner.prototype.clean = function (building, roomName) {
        // Array < String, String > hash = new Array<>();
        // hash.push("building", building);
        // hash.push("roomName", roomName);
        // resultByHuman.add(hash);
        //System.out.println("clear Method building"+ building+ "roomName" + roomName);
        //System.out.println("building :"+building+"roomName"+roomName);
    };
    OfficeCleaner.prototype.getCleanedRoom = function () {
        this.buildingObj = { buildingName: 'Software Park', roomName: 'room 301' };
        return this.buildingObj;
    };
    OfficeCleaner.prototype.buyBaygon = function (numBaygon) {
        console.log("Baygon : " + numBaygon);
    };
    OfficeCleaner.prototype.killCoachroach = function (baygonNumber, building, roomName) {
        console.log("killCoachroach " + baygonNumber + " " + building + " " + roomName);
    };
    OfficeCleaner.prototype.getTotalKilled = function () {
        this.totalKilled += 100;
        this.killCoachroach(100, " building ", " roomName ");
        return this.totalKilled;
    };
    OfficeCleaner.prototype.CleanedRoom = function () {
        console.log("method clean room");
    };
    return OfficeCleaner;
}(Employee_1.Employee));
exports.OfficeCleaner = OfficeCleaner;
//# sourceMappingURL=OfficeCleaner.js.map