"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Employee = /** @class */ (function () {
    function Employee(firstname, lastname, salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }
    Employee.prototype.hello = function () {
        console.log("Employee hello");
    };
    return Employee;
}());
exports.Employee = Employee;
//# sourceMappingURL=Employee.js.map