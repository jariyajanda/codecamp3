"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var text;
text = 'Hello World';
console.log(text);
var num = 25;
var studentName = 'Codecamp';
var arr = ['a', 'b'];
var x = 20;
var y = 25;
console.log(num);
console.log(studentName);
console.log(arr);
console.log(x);
console.log(y);
function calculator(x, y) {
    return x + y;
}
console.log(calculator(x, y));
var items;
items = [];
items.push(1);
items.push(2);
console.log(items);
var Animal = /** @class */ (function () {
    function Animal() {
    }
    return Animal;
}());
var Bird = /** @class */ (function (_super) {
    __extends(Bird, _super);
    function Bird(name) {
        var _this = _super.call(this) || this;
        _this.name = name;
        return _this;
    }
    Bird.prototype.myName = function () {
        console.log(" My name is " + this.name + " ");
    };
    Bird.prototype.fly = function () {
        console.log('I’m flying');
    };
    return Bird;
}(Animal));
var AA;
AA = new Bird("bird one");
AA.name = "jjjjj";
AA.myName();
AA.coler = "red";
AA.fly();
var BB;
BB = new Bird("bird two");
BB.fly();
var student_list_1 = require("./student-list");
var cal = new student_list_1.Calculator();
cal.calculate(1, 2);
for (var _i = 0, student_1 = student_list_1.student; _i < student_1.length; _i++) {
    var items_1 = student_1[_i];
    console.log('student ' + items_1);
}
//# sourceMappingURL=app.js.map