"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Employee_1 = require("./Employee");
var CEO = /** @class */ (function (_super) {
    __extends(CEO, _super);
    function CEO(firstname, lastname, salary) {
        return _super.call(this, firstname, lastname, salary) || this;
        // this.firstname = firstname;
        // this.lastname = lastname;
        // this.salary = salary;
    }
    CEO.prototype.hello = function () {
        console.log("Hi, nice to meet you. " + this.firstname + " !");
    };
    CEO.prototype.orderWebsite = function (creator) {
        creator.createWebsite("some template", "Codecamp3");
    };
    CEO.prototype.installWindows = function (version, productKey) {
        return "versin is " + version + " prodoct key is " + productKey;
    };
    CEO.prototype.orderKillCoachroach = function (clear) {
        clear.killCoachroach(100, "building", "roomName");
    };
    CEO.prototype.orderCleaned = function (cleaner, building, roomName) {
        cleaner.clean(building, roomName);
    };
    return CEO;
}(Employee_1.Employee));
exports.CEO = CEO;
//# sourceMappingURL=CEO.js.map