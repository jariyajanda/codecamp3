"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Employee_1 = require("./Employee");
var Programmer = /** @class */ (function (_super) {
    __extends(Programmer, _super);
    function Programmer(firstname, lastname, salary) {
        return _super.call(this, firstname, lastname, salary) || this;
        // this.firstname = firstname;
        // this.lastname = lastname;
        // this.salary = salary;
    }
    Programmer.prototype.fixPC = function (serialNumber) {
        console.log("I'm trying to fix PC serialNumber: " + serialNumber);
    };
    Programmer.prototype.createWebsite = function (template, titleName) {
        console.log("Create website by Programmer : " + titleName + " " + template);
    };
    Programmer.prototype.installWindows = function (version, productKey) {
        console.log("version: " + version + " product: " + productKey);
    };
    Programmer.prototype.formatWindows = function (drive) {
        console.log("Format windows : " + drive);
    };
    Programmer.prototype.LastInstalledWindowsVersion = function () {
        return 10;
    };
    return Programmer;
}(Employee_1.Employee));
exports.Programmer = Programmer;
//# sourceMappingURL=Programmer.js.map