package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String password = req.getParameter("password");
        String username = req.getParameter("username");
        Cookie cookie = new Cookie("username", username);

        try {
            InputStream stream = this.getServletContext().getResourceAsStream("WEB-INF/single_employee.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                // System.out.println(stringBuilder);
            }

            reader.close();
            // TODO
            // String json to List Object Read File
            // Input Check List Object
            // if
            String jsonContent = stringBuilder.toString();
            ObjectMapper objectMapper = new ObjectMapper();
            Employee employee = objectMapper.readValue(jsonContent, Employee.class);

            System.out.println(employee.toString());
            String getUser = employee.getUsername();
            String getPass = employee.getPassword();
            String x = "username/password is not correct";
            if (getUser.equals(username) && getPass.equals(password)) {
                String first = employee.getFirstname();
                String last = employee.getLastname();
                HttpSession session = req.getSession();
                session.setAttribute("FirstName", first);
                session.setAttribute("LastName", last);
                session.setMaxInactiveInterval(30);
                resp.sendRedirect("member");
            } else {
                req.setAttribute("result", x);
                req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);

                // resp.getWriter().write(x);
                // resp.sendRedirect("login");
                // System.out.println("username/password is not correct");

            }

        } catch (Exception error) {
            System.out.println(error.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
    }

}
