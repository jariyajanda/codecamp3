package web;

public class Job {
    private int id;
    private String jobname;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the jobname
     */
    public String getJobname() {
        return jobname;
    }

    /**
     * @param jobname the jobname to set
     */
    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

}
