package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class EmployeeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/exam", "root", "password");
            Statement statement = connection.createStatement();
            String strsql = "select employee.id, employee.firstname, employee.lastname,job.job_name from employee left join job on job.id = employee.job_id";
            ResultSet resultSet = statement.executeQuery(strsql);

            ArrayList<Employee> Ins = new ArrayList<Employee>();
            while (resultSet.next()) {

                Employee emp = new Employee();
                int strid = resultSet.getInt("employee.id");
                String strname = resultSet.getString("employee.firstname");
                String strlast = resultSet.getString("employee.lastname");
                String strjob = resultSet.getString("job.job_name");
                emp.setId(strid);
                emp.setFirstname(strname);
                emp.setLastname(strlast);
                emp.setJobname(strjob);
                Ins.add(emp);
            }
            statement.close();
            connection.close();
            req.setAttribute("Ins", Ins);
            req.getRequestDispatcher("/jsp/employee.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
