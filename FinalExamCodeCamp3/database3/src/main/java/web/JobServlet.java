package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class JobServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/exam", "root", "password");
            Statement statement = connection.createStatement();
            String strsql = "select job.id,job.job_name from job left join employee on employee.job_id = job.id where job_id is null";
            ResultSet resultSet = statement.executeQuery(strsql);

            ArrayList<Job> job = new ArrayList<Job>();
            while (resultSet.next()) {

                Job cos = new Job();
                int id = resultSet.getInt("job.id");
                String strname = resultSet.getString("job.job_name");
                cos.setId(id);
                cos.setJobname(strname);

                job.add(cos);
            }
            statement.close();
            connection.close();
            req.setAttribute("job", job);
            req.getRequestDispatcher("/jsp/jobisnull.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
