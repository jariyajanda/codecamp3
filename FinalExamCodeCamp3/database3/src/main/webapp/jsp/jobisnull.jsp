<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
</head>

<body>
    <table border="1">
        <tr>
            <th>ID Job</th>
            <th>Job Name</th>
        </tr>
        <c:forEach var="item" items="${job}">
            <tr>
                <td>
                    <c:out value="${item.getId()}" />
                </td>
                <td>
                    <c:out value="${item.getJobname()}" />
                </td>
            </tr>
        </c:forEach>
    </table>
</body>

</html>