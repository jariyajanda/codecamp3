<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
</head>

<body>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Job Name</th>
            <th>Image</th>
        </tr>
        <c:forEach var="item" items="${Ins}">
            <tr>
                <td>
                    <c:out value="${item.getId()}" />
                </td>
                <td>
                    <c:out value="${item.getFirstname()}" />
                </td>
                <td>
                    <c:out value="${item.getLastname()}" />
                </td>
                <td>
                    <c:out value="${item.getJobname()}" />
                </td>

                <td>

                    <img src="/image/${item.getJobname()}.jpg">

                </td>
            </tr>
        </c:forEach>
    </table>
</body>

</html>