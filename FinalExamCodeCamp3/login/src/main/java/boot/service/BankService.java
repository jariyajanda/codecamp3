package boot.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.dao.BankDao;
import boot.entity.PersonEntity;

@Service
public class BankService {

    @Autowired
    private BankDao bankDao;

    public PersonEntity openAccount(String accountNo, String accountType, String personId, String personFullName,
            BigDecimal openingBalance) {
        return bankDao.openAccount(accountNo, accountType, personId, personFullName, openingBalance);
    }

}
