package boot.controller;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import boot.entity.BankAccountRequest;
import boot.entity.Person;
import boot.entity.PersonEntity;
import boot.service.BankService;
import boot.util.DtoConverter;

@RestController
public class BankController {

	@Autowired
	BankService bankService;

	@PostMapping(path = "/camp3/api/openAccount", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Person> openAccount(@RequestBody BankAccountRequest bankAccountRequest) {
		PersonEntity person = bankService.openAccount(bankAccountRequest.getAccountNo(),
				bankAccountRequest.getAccountType(), bankAccountRequest.getPersonId(),
				bankAccountRequest.getPersonFullName(), bankAccountRequest.getAmount());
		List<PersonEntity> people = Arrays.asList(person);
		return new ResponseEntity<Person>(DtoConverter.getPersonDto(people), HttpStatus.OK);
	}

}
