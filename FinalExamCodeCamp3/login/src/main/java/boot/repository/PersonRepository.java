package boot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import boot.entity.PersonEntity;
import boot.entity.PersonId;

@Repository
public interface PersonRepository extends CrudRepository<PersonEntity, PersonId> {
}
