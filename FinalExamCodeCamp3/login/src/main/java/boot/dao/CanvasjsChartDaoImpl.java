package boot.dao;
 
import java.util.List;
import java.util.Map;

import boot.model.CanvasjsChartData;
 
public class CanvasjsChartDaoImpl implements CanvasjsChartDao {
 
	@Override
	public List<List<Map<Object, Object>>> getCanvasjsChartData() {
		return CanvasjsChartData.getCanvasjsDataList();
	}
 
}  