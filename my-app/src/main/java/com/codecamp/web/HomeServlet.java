package com.codecamp.web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
// import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       // ServletOutputStream outputStream = resp.getOutputStream();

        ArrayList<Students> students = new ArrayList<Students>();

       for (int i = 1; i <= 10; i++) {

           Students student = new Students();
           student.setFirstname(String.format("Mr. %s", i));
           student.setLastname(String.format("Number %s", i));

           students.add(student);
       }

       req.setAttribute("students", students);
       req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);



    // ServletOutputStream outputStream = resp.getOutputStream();
    //    req.setAttribute("result", "OK");
    //    req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);

    //    outputStream.print("<h1>Hello world - Codecamp 3</h1>");
    //    outputStream.print("<h3>This is my first servlet</h3>");
        // outputStream.flush();
        // outputStream.close();
   }
}
