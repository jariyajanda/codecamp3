$.get('homework3_1.json', function (jsonResult) {

    let employees = jsonResult;
    console.log(employees);
    //console.log(jsonResult);
    /*
        for(let i=0; i<employees.length;i++){
            convertNumber(employees[i]);
            addYearSalary(employees[i]);
            addNextSalary(employees[i]);
        }
    */

    let emps = addAdditionalFields(employees)

});
// function นี้ แปลงค่า เงินเดือนจาก string เป็น int
function convertNumber(row) {
    //console.log(row)
    row.salary = parseInt(row["salary"])
}
// function หาเงินเดือนต่อปี โดยเอา เงินเดือนมา*12
function addYearSalary(row) {

    row.yearSalary = row["salary"] * 12
}
//function หาเงินเดือน เดือนถัดไป *1.1 ของเงินเดือนล่าสุด 
function addNextSalary(row) {
    //console.log(row)
    let tempSalary = parseInt(row["salary"])
    let salary = [];
    salary.push(tempSalary)
    // วนลูปเงินเดือนแต่ล่ะเดือน *1.1 เก็บใน salary 
    for (let i = 1; i < 3; i++) {
        tempSalary = parseInt(tempSalary * 1.1)
        salary.push(tempSalary)
    }
    // เอา salary ที่*1.1 เก็บใน row.nextsalary
    row.nextSalary = salary
}
// function นี้เรียก การทำงานของ function ต่างๆๆ โดยส่งค่า employee เข้ามาใน parameter
function addAdditionalFields(emp) {
    let htmlDataTable = '';
    for (let i = 0; i < emp.length; i++) {

        convertNumber(emp[i]);
        addYearSalary(emp[i]);
        addNextSalary(emp[i]);

        let colId = '<td>' + emp[i].id + '</td>';
        let colFristname = '<td>' + emp[i].fristname + '</td>';
        let colLastname = '<td>' + emp[i].lastname + '</td>';
        let colCompany = '<td>' + emp[i].company + '</td>';
        let colSaraly = '<td>' + emp[i].salary + '</td>';
        text = "<ol>";
        for (let k = 0; k < 3; k++) {
            text += "<li>" + emp[i]["nextSalary"][k] + "</li>";

        }
        text += "</ol>";
        let colYearSaraly = '<td>' + emp[i].yearSalary + '</td>';
        let colNextSaraly = '<td>' + text + '</td>';
        let rowHtml = '<tr>' +
            colId +
            colFristname +
            colLastname +
            colCompany +
            colSaraly +
            colYearSaraly +
            colNextSaraly
            + '</tr>'

        htmlDataTable += rowHtml

    }

    $('#myTable').append(htmlDataTable);
}