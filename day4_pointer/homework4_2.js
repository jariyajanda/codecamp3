$.get('homework3_1.json', function (jsonResult) {

    let employees = jsonResult;
    let newEmployees = addAdditionalFields(employees);
    console.log(employees);
    newEmployees[0].id = 0
    console.log(newEmployees)

    tableOld(employees)
    tableNew(newEmployees)

});
function convertNumber(row) {
    row.salary = parseInt(row["salary"])
}

function addYearSalary(row) {
    row.yearSalary = row["salary"] * 12
}

function addNextSalary(row) {
    let tempSalary = parseInt(row["salary"])
    let salary = [];
    salary.push(tempSalary)
    for (let i = 1; i < 3; i++) {
        tempSalary = parseInt(tempSalary * 1.1)
        salary.push(tempSalary)
    }
    row.nextSalary = salary
}
//function clone object เพื่อได้ object ก้อนใหม่
function copyEmployees(src) {
    let target = {};
    for (let prop in src) {
        if (src.hasOwnProperty(prop)) {
            target[prop] = src[prop];
        }
    }
    return target;
}

function addAdditionalFields(obj) {
    //let htmlDataTable = '';
    let listemp = [];

    for (let i = 0; i < obj.length; i++) {

        let emp = copyEmployees(obj[i]);
        convertNumber(emp);
        addYearSalary(emp);
        addNextSalary(emp);

        listemp.push(emp)
    }
    return listemp
    /*
    let colId = '<td>'+emp[i].id+'</td>';
    let colFristname = '<td>'+emp[i].fristname+'</td>';
    let colLastname = '<td>'+emp[i].lastname+'</td>';
    let colCompany = '<td>'+emp[i].company+'</td>';
    let colSaraly = '<td>'+emp[i].salary+'</td>';
    text = "<ol>";
       for (let k = 0; k < 3; k++) {
       text += "<li>"+emp[i]["nextSalary"][k] +"</li>";
   
   }
    text += "</ol>";
    let colYearSaraly = '<td>'+emp[i].yearSalary+'</td>';
    let colNextSaraly = '<td>'+text+'</td>';
    let rowHtml = '<tr>'+
    colId+
    colFristname+
    colLastname+
    colCompany+
    colSaraly+
    colYearSaraly+
    colNextSaraly
    +'</tr>'

    htmlDataTable += rowHtml
   
   } 

   $('#myTable').append(htmlDataTable);
   */
}
function tableNew(emps) {
    let htmlDataTable = '';
    for (let i = 0; i < emps.length; i++) {
        let colId = '<td>' + emps[i].id + '</td>';
        let colFristname = '<td>' + emps[i].fristname + '</td>';
        let colLastname = '<td>' + emps[i].lastname + '</td>';
        let colCompany = '<td>' + emps[i].company + '</td>';
        let colSaraly = '<td>' + emps[i].salary + '</td>';
        text = "<ol>";
        for (let k = 0; k < 3; k++) {
            text += "<li>" + emps[i]["nextSalary"][k] + "</li>";
        }
        text += "</ol>";
        let colYearSaraly = '<td>' + emps[i].yearSalary + '</td>';
        let colNextSaraly = '<td>' + text + '</td>';
        let rowHtml = '<tr>' + colId + colFristname + colLastname + colCompany + colSaraly + colYearSaraly + colNextSaraly + '</tr>'

        htmlDataTable += rowHtml
    }
    $('#myTableNew').append(htmlDataTable);
}
function tableOld(emps) {
    let htmlDataTable = '';
    for (let i = 0; i < emps.length; i++) {
        let colId = '<td>' + emps[i].id + '</td>';
        let colFristname = '<td>' + emps[i].fristname + '</td>';
        let colLastname = '<td>' + emps[i].lastname + '</td>';
        let colCompany = '<td>' + emps[i].company + '</td>';
        let colSaraly = '<td>' + emps[i].salary + '</td>';

        let rowHtml = '<tr>' + colId + colFristname + colLastname + colCompany + colSaraly + '</tr>'
        htmlDataTable += rowHtml
    }
    $('#myTableOld').append(htmlDataTable);

}