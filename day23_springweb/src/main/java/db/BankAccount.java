package db;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bank_account") // bank_account == ชื่อตารางใน database
public class BankAccount {
    // @Id == pk in table database table bank_account
    @Id
    @Column(name = "account_no", unique = true)
    private String accountNo;
    @Column(name = "balance")
    private BigDecimal balance;
    @Column(name = "status")
    private Integer status;

    public BankAccount() {
    }

    public BankAccount(String accountNo, BigDecimal balance, Integer status) {
        this.accountNo = accountNo;
        this.balance = balance;
        this.status = status;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Integer getStatus() {
        return status;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "[ACC_NO=" + accountNo + ", BALANCE=" + balance + ", STATUS=" + status + "]";
    }

}
