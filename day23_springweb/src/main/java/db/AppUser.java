package db;

import java.io.Serializable;

public class AppUser implements Serializable {
    private String gender;
    private Integer user_id;
    private String firstname;
    private String lastName;

    public AppUser() {

    }

    public AppUser(Integer user_id, String firstname, String lastName, String gender) {
        this.user_id = user_id;
        this.firstname = firstname;
        this.lastName = lastName;
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}