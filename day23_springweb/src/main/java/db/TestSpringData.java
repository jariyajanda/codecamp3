package db;

import java.math.BigDecimal;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestSpringData {
    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(DbConfig.class)) {
            PersonRepository personRepository = ctx.getBean(PersonRepository.class);
            BankAccount bankAccount1 = new BankAccount("ACC_1", BigDecimal.valueOf(500.00), 1);
            BankAccount bankAccount2 = new BankAccount("ACC_2", BigDecimal.valueOf(500.00), 1);
            BankAccount bankAccount3 = new BankAccount("ACC_3", BigDecimal.valueOf(500.00), 1);
            Person person1 = new Person("CUST1", bankAccount1, "Somchai Kemklud", 1);
            Person person2 = new Person("CUST2", bankAccount2, "Somchai Kemklud", 1);
            Person person3 = new Person("CUST3", bankAccount3, "Mai Dawika", 1);
            BankService.openAccount(person1);
            BankService.openAccount(person2);
            BankService.openAccount(person3);
            BankAccount bankAccount = new BankAccount();
            bankAccount.setAccountNo("ACC_1");
    
            BankService.deposit(bankAccount1, BigDecimal.valueOf(100.00));
        }
    }
}