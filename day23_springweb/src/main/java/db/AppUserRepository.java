package db;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
public interface AppUserRepository extends CrudRepository<AppUser, Long>  {
    public AppUser findByUserName(String userName);
 
    @Query(
    value = "SELECT * FROM app_user WHERE gender = ?1",
    nativeQuery = true)
    public Collection<AppUser> findByGender(String email);
 
    @Query(
    value = "SELECT * FROM app_user WHERE user_id = ?1",
    nativeQuery = true)
    public AppUser findByMyId(Long id);
 
    @Query(
    value = "SELECT * FROM app_user WHERE firstname = ?1 AND lastname = ?2",
    nativeQuery = true)
    public AppUser findByFullName(String firstName, String lastName);
 }
 