CREATE TABLE supplier
(
    name varchar(255) NOT NULL,
    address varchar(255),
    phone_number int(10),
    PRIMARY KEY(name)
);


CREATE TABLE product
(
    id int
    auto_increment,
                    name varchar
    (255) NOT NULL,
                    detail varchar
    (255),
                    price decimal
    (10,2),
                    quantity int
    (10),
                    PRIMARY KEY
    (id),
                    FOREIGN KEY
    (name) REFERENCES supplier
    (name));

    CREATE TABLE department
    (
        name varchar(255),
        budget decimal(65,2),
        PRIMARY KEY(name)
    );


    CREATE TABLE costomer
    (
        id int
        auto_increment,
                    name varchar
        (255) NOT NULL,
                    address varchar
        (255),
                    PRIMARY KEY
        (id));

        CREATE TABLE employee
        (
            id int
            auto_increment,
                    name varchar
            (255) NOT NULL,
                    address varchar
            (255),
                    salary decimal
            (65,2),
                    depart_name VARCHAR
            (255),
                    PRIMARY KEY
            (id),
                    FOREIGN KEY
            (depart_name) REFERENCES department
            (name));
            CREATE TABLE orders
            (
                id int
                auto_increment,
                     datetime TIMESTAMP ,
                     sum_amount decimal
                (65,2),
                     cos_id int,
                     emp_id int,
                    PRIMARY KEY
                (id),
                    FOREIGN KEY
                (cos_id) REFERENCES costomer
                (id),
                    FOREIGN KEY
                (emp_id) REFERENCES employee
                (id));

                CREATE TABLE order_item
                (
                    id int
                    auto_increment,
                    amount decimal
                    (65,2),
                    discount decimal
                    (65,2),
                    order_id int,
                    product_id int,
                    datetime TIMESTAMP ,
                    PRIMARY KEY
                    (id),
                    FOREIGN KEY
                    (order_id) REFERENCES orders
                    (id),
                    FOREIGN KEY
                    (product_id) REFERENCES product
                    (id));
                   