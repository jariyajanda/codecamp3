package day_11;

public class Employee {
    private String firstname;
    private String lastname;
    private int salary;
    private String company;
    private int id;
 
 public Employee () {
     this.salary = 1000;
    }
    public Employee (int id,String firstname,String lastname, int salary,String company) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
        this.id = id;
        this.company = company;
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastname(){
        return lastname;
    }
    public void setLastname(String lastname){
        this.lastname = lastname;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        if (salary > 0)
        this.salary = salary;
    }
    public int getID(){
        return id;
    }
    public void setID(int id){
        this.id = id;
    }
    public String getCompany(){
        return company;
    }
    public void setCompany(String company){
        this.company = company;
    }
 }
 