package day_11;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Homework11_1 {
    public static void main(String[] args) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            Employee[] employees = objectMapper.readValue(new File("target/Homework3_1.json"), Employee[].class);
            for (int i = 0; i < employees.length; i++) {
                System.out.println("id : " + employees[i].getID()); // Dang
                System.out.println("name : " + employees[i].getFirstname());
                System.out.println("last : " + employees[i].getLastname());
                System.out.println("company" + employees[i].getCompany());
                System.out.println("salary : " + employees[i].getSalary());
            }
        } catch (IOException ex) {
            System.out.println("JSON parse to Employee object error " + ex.getMessage());
        }

    }
}