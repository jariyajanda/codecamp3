package day_11;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Homework11_2 {

  public static void main(String[] args) {

    try {

      ObjectMapper objectMapper = new ObjectMapper();
      ArrayList<HashMap<String, String>> employees = objectMapper.readValue(
          new URL("https://jsonplaceholder.typicode.com/posts"),
          new TypeReference<ArrayList<HashMap<String, String>>>() {
          });

      for (int i = 0; i < employees.size(); i++) {
        System.out.println("userId: " + employees.get(i).get("userId"));
        System.out.println("id: " + employees.get(i).get("id"));
        System.out.println("title: " + employees.get(i).get("title"));
        System.out.println("body: " + employees.get(i).get("body"));

      }

      // ObjectMapper objectMapper = new ObjectMapper();
      ArrayList<Post> posts = objectMapper.readValue(new URL("https://jsonplaceholder.typicode.com/posts"),
          new TypeReference<List<Post>>() {
          });
      int size = posts.size();
      for (int i = 0; i < size; i++) {
        System.out.println("userId : " + posts.get(i).getUserId());
        System.out.println("id : " + posts.get(i).getId());
        System.out.println("title: " + posts.get(i).getTitle());
        System.out.println("body: " + posts.get(i).getBody());
      }

    } catch (IOException ex) {
      System.out.println("JSON parse error: " + ex.getMessage());
    }

  }
}
