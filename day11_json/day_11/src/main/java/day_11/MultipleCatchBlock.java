package day_11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference; // พิมพ์ไว้นอก Class
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

class MultipleCatchBlock {

    public static void main(String[] args) {
        
      try {
        int a[] = new int[5];
        a[10] = 25 / 5;
      } catch (ArithmeticException e) {
        System.out.println("Arithmetic Exception occurs");
      } catch (ArrayIndexOutOfBoundsException e) {
        System.out.println("ArrayIndexOutOfBounds Exception occurs");
      } catch (Exception e) {
        System.out.println("Parent Exception occurs");
      }
      
      System.out.println("rest of the code");
      
      /*
      try {
        int data = 25 / 5;
        System.out.println(data);
      } finally {
        System.out.println("finally block is executed");
      }
      System.out.println("rest of the code...");
      */
      /*
      try {
        int data = 25 / 0;
        System.out.println(data);
      } catch (ArithmeticException e) {
        System.out.println(e);
      } finally {
        System.out.println("finally block is executed");
      }
      System.out.println("rest of the code...");
    */
    /*
    int peopleAge = 13;
     validateAge(peopleAge);
    }
    static void validateAge(int age){
        if (age < 18)
            throw new ArithmeticException("Invalid age");
        else
            System.out.println("Valid to vote");
    }
*/
/*
    try {
        callMe();
    } catch (IllegalAccessException e) {
        System.out.println(e);
        System.out.println("caught in main.");
    }
    }
    static void callMe() throws IllegalAccessException {
    System.out.println("Inside callMe(). ");
    throw new IllegalAccessException("demo");
*/
/*
    try {
      FileWriter myWriter = new FileWriter("day11/filename.txt");
      myWriter.write("Some Content in filename.txt");
      myWriter.close();
      System.out.println("Successfully wrote to the file.");
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
*/
/*
    try {
      File myObj = new File("day11/filename.txt");
      Scanner myReader = new Scanner(myObj);
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        System.out.println(data); // พิมพ์ข้อความใน filename.txt ออกมาทีละบรรทัด
      }
      myReader.close();
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
 */
    /*
    ObjectMapper objectMapper = new ObjectMapper();
    Employee dang = new Employee(1001, "Dang", "Red", 10000);

    try {
    objectMapper.writeValue(new File("day11/employee.json"), dang);
    } catch (IOException ex) {
    System.out.println("There's something wrong with target/employee.json " + 
    ex.getMessage());
    }
*/
/*
ObjectMapper objectMapper = new ObjectMapper();
Employee dang = new Employee(1001, "Dang", "Red", 10000);

try {
objectMapper.writeValue(new File("target/employee.json"), dang);
} catch (IOException ex) {
System.out.println("There's something wrong with target/employee.json " + 
ex.getMessage());
}
*/
/*
ObjectMapper objectMapper = new ObjectMapper();
Employee dang = new Employee(1001, "Dang", "Red", 10000);

try {
  String employeeAsString = objectMapper.writeValueAsString(dang);
  System.out.println(employeeAsString);
// {"id":1001,"firstname":"Dang","lastname":"Red","salary":10000}
} catch (JsonProcessingException ex) {
System.out.println("There's something wrong with writeValueAsString: " + 
ex.getMessage());
}
*/
/*
ObjectMapper objectMapper = new ObjectMapper();
Employee dang = new Employee(1001, "Dang", "Red", 10000);
try{
  Employee dang2 = objectMapper.readValue(new File("target/employee.json"), Employee.class);
 System.out.println(dang2.getLastname()); // Dang
} catch (IOException ex) {
  System.out.println("JSON parse to Employee object error " + ex.getMessage());
}
*/
/*
    ObjectMapper objectMapper = new ObjectMapper();
    Employee dang = new Employee(1001, "Dang", "Red", 10000);
    try{
      Employee dang2 = objectMapper.readValue(new URL("file://target/employee.json"), Employee.class);
    System.out.println(dang2.getFirstname()); // Dang
    } catch (IOException ex) {
      System.out.println("JSON parse to Employee object error " + ex.getMessage());
    }
*/
/*
    String json = "{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 }";
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      Employee dang2 = objectMapper.readValue(json, Employee.class);;           
      System.out.println(dang2.getFirstname());
    } catch (IOException ex) {
      System.out.println("JSON parse error: " + json + ex.getMessage());
    }

    */
    /*
    String jsonEmployeeArray = "[{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 },{ \"id\" : 1002, \"firstname\" : \"Captain2\", \"lastname\" : \"Marvel2\", \"salary\" : 30000 }]";
    ObjectMapper objectMapper = new ObjectMapper();
    try{
      List<Employee> employees = objectMapper.readValue(jsonEmployeeArray, new 
    TypeReference<List<Employee>>(){});
      String firstname = employees.get(1).getFirstname();
      System.out.println(firstname);
    } catch (IOException ex) {
      System.out.println("JSON parse error: " + jsonEmployeeArray + ex.getMessage());
    }
*/
/*
    String json2 = "{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 }";
    ObjectMapper objectMapper = new ObjectMapper();
    try{
      HashMap<String, Object> map = objectMapper.readValue(json2, new 
    TypeReference<HashMap<String,Object>>(){});
      System.out.println(map.get("firstname")); // Captain
    } catch (IOException ex) {
      System.out.println("JSON parse error :" + json2 + ex.getMessage());
    }
*/
/*
String jsonEmployeeArray2 = "[{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 },{ \"id\" : 1002, \"firstname\" : \"Captain2\", \"lastname\" : \"Marvel2\", \"salary\" : 30000 }]";
ObjectMapper objectMapper = new ObjectMapper();
try{
   objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
   Employee[] employees = objectMapper.readValue(jsonEmployeeArray2, Employee[].class);
   System.out.println(employees[1].getFirstname()); // Captain2
} catch (IOException ex) {
   System.out.println("JSON parse error " + jsonEmployeeArray2 + ex.getMessage());
}
*/
/*

String jsonEmployeeArray3 = "[{ \"id\" : 1001, \"firstname\" : \"Captain\", \"lastname\" : \"Marvel\", \"salary\" : 30000 },{ \"id\" : 1002, \"firstname\" : \"Captain2\", \"lastname\" : \"Marvel2\", \"salary\" : 30000 }]";
ObjectMapper objectMapper = new ObjectMapper();
try{
   ArrayList<HashMap<String, String>> employees = objectMapper.readValue(jsonEmployeeArray3, new 
TypeReference<ArrayList<HashMap<String, String>>>(){});
   String firstname = employees.get(1).get("id"); // 1002
   System.out.println(firstname);
} catch (IOException ex) {
   System.out.println("JSON parse error: " + jsonEmployeeArray3 + ex.getMessage());
}
*/

try{
  String json3 = "{ \"name\" : \"Dang\", \"innerObject\" : { \"keyInside2\" : { \"keyInside3\" : 123 } } }";
  ObjectMapper objectMapper = new ObjectMapper();
  JsonNode jsonNode = objectMapper.readTree(json3);
  String name = jsonNode.get("name").textValue();
  System.out.println(name); // Dang
  int valueInside = jsonNode.get("innerObject").get("keyInside2").get("keyInside3").intValue();
  System.out.println(valueInside); // 123
} catch (IOException ex) {
  System.out.println("JSON parse to Employee object error " + ex.getMessage());
}


    }
}

   