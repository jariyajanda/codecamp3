<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
</head>

<body>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
        <c:forEach var="item" items="${students}">
            <tr>
                <td>
                    <c:out value="${item.studentid}" />
                </td>
                <td>
                    <c:out value="${item.firstname}" />
                </td>
                <td>
                    <c:out value="${item.lastname}" />
                </td>
            </tr>
        </c:forEach>
    </table>
</body>

</html>