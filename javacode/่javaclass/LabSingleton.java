import com.sun.org.apache.regexp.internal.recompile;

public class LabSingleton {
    public static void main(String[] args) {
        double sellPrice = 10000.00;
        double vat = LabSingleton.calVat(sellPrice);
        double calIncludevat = LabSingleton.calIncludeVat(sellPrice);
        double wht = LabSingleton.calWHT(sellPrice);
        System.out.println("sellPirce " + sellPrice);
        System.out.println("vat " + vat);
        System.out.println("Sum price + vat 7% : "+calIncludevat);
        System.out.println("wht " + LabSingleton.calWHT(sellPrice));
        System.out.println("Sum price - WHT 3% : "+(calIncludevat-wht));
    }

    public static double calVat(double aml) {
        double res = aml * 7 / 100;

        return res;
    }

    public static double calIncludeVat(double aml){
        double res = aml * 7 / 100;
        double sum = res + aml;
        return sum;
    }

    public static double calWHT(double aml){
        double res = aml * 3/100;
       
        return res;
    }
}