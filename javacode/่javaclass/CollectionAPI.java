import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionAPI {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("aa");
        System.out.println(list.get(0));

        List<Customer> cust = new ArrayList<>();
        cust.add(new Customer("123","name1","email1"));
        cust.add(new Customer("124","name2","email2"));
        cust.add(new Customer("124","name3","email3"));
        for (Customer c : cust) {
         System.out.println(c.getId());
         System.out.println(c.getName());
         System.out.println(c.getEmail());

        }
        Set<String> collset = new HashSet();
        collset.add("hello");
        collset.add("how");
        collset.add("are");
        collset.add("you");
        collset.add("hello");
        collset.add("how");
        collset.add("are");
        collset.add("you");

        for (String s : collset) {
            System.out.println(s);
        }
        System.out.println("------------------");
        String [] strs = collset.toArray(new String[0]);
        for (String sts : strs) {
            System.out.println(sts);
        }
        System.out.println(strs[0]);

        Map<String,Customer> myMap = new HashMap();
        myMap.put("11", new Customer("11","name11","email11"));
        myMap.put("11", new Customer("11","name22","email22"));
        myMap.put("22", new Customer("22","name11","email11"));
        Customer c = myMap.get("11");
        System.out.println(c.getId()+" "+c.getName());
        for(String key : myMap.keySet()){
            System.out.println("key : "+key+" value : "+myMap.get(key).getName());
        }
    }
  
}
class Customer{
    private String id;
    private String name;
    private String email;

    public Customer(String id,String name,String email){
        this.id = id;
        this.name = name;
        this.email = email;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}