public class Singleton{
    public static void main(String[] args) {
        // HouseCreator hc = HouseCreator.getInstance();
        // House h = hc.createHouse("ad","name","green");
        // System.out.println(h);
    }

    public void createHouse(){
        House h;
        for(int i =1; i < 100; i++){
            h  = new House("address"+i+"color : "+i);
            System.out.println(h);
            System.out.println("id : "+h.getAddress()+" color "+h.getColor());
        }
    }
}

class HouseCreator{
    private static HouseCreator instance = new HouseCreator();
    private HouseCreator(){

    }
    public static HouseCreator getInstance(){
        return instance;
    }
    public House createHouse(String address,String name , String color){

        return new House(address,name,color);
    }
}
class House{
    private String address;
    private String name;
    private String color;

    public House(){

    }
    public House(String address,String name,String color){
        this.address = address;
        this.name = name;
        this.color = color;

    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
}