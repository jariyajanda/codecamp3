import com.sun.org.apache.regexp.internal.recompile;

public class StringAPI {
    public static void main(String[] args) {
        // MyString ms = new MyString();

        // String cv = ms.convert("Hello World");
        // System.out.println(cv);

        // String en = ms.encode("Hello");
        // System.out.println("Hello " + en);
        // String de = ms.decode(en);
        // System.out.println(en + " decode :  " + de);

        // System.out.println(ms.search("Hello world", "or"));

        // System.out.println(ms.rmSpace(" hello  world "));
        // ms.cont("How are you today");
    }
}

class MyString {

    public int cont(String input) {
        int res = 0;
        System.out.println(input);
        for (int i = 0; i < input.length(); i++) {
           char c = input.charAt(i);
            if(c == "o"){
                res += 1;
            }
        //    if(c == "re"){
        //        res +=1;
        //    }
        }
        return res;
    }

    public String rmSpace(String input) {
        String tim = input.trim();

        return tim;
    }

    public String convert(String input) {
        Mystack mystack = new Mystack();
        System.out.println(input);
        for (int i = 0; i < input.length(); i++) {
            // System.out.print(input.charAt(i));
            char c = input.charAt(i);
            mystack.push("" + c);
        }
        String res = "";

        while (mystack.getSize() >= 0) {

            res += mystack.pop();
        }
        return res;
    }

    // public String convert(String input){
    // String res = "";
    // for(int i = input.length(); i > 0 ; i --){
    // res = res + input.charAt(i-1);
    // }
    // return res;
    // }
    public String encode(String input) {
        int nextEn = 100;
        String res = "";
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            c += nextEn;
            res += c;
        }
        return res;
    }

    public String decode(String input) {
        int nextEn = 100;
        String res = "";
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            c -= nextEn;
            res += c;
        }
        return res;
    }

    public boolean search(String src, String input) {
        return src.contains(input);
    }
}

class Mystack {
    private String[] item = new String[100];
    private int index = 0;

    public void push(String input) {
        item[index] = input;
        index++;
    }

    public String pop() {
        // index--;
        String s = item[index--];
        return s;
    }

    public int getSize() {
        return index;
    }
}