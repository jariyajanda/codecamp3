<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java"
   contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>

<html>

<head>
    <title> Registration Form</title>
</head>

<body>
    <h2> Registration Form</h2>
    <form:form method="POST" action="/camp3/registerFrom">
        <table>
            <tr>
                <td>
                    <form:label path="firstName">Name</form:label>
                </td>
                <td>
                    <form:input path="firstName" />
                    <form:errors path="firstName" />

                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="lastName">Lastname</form:label>
                </td>
                <td>
                    <form:input path="lastName" />
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="email">E-mail</form:label>
                </td>
                <td>
                    <form:input path="email" cssClass="error" />
                    <p>${exceptionaa}</p>

                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Submit" />
                </td>
            </tr>
        </table>
    </form:form>
</body>

</html>