<%@page contentType = "text/html;charset = UTF-8" language = "java" %>
<%@page isELIgnored = "false" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>

<head>
    <title>Result</title>
</head>

<body>
    <h2>Submitted Information</h2>
    <table>
        <tr>
            <td>Name</td>
            <td>${firstName}</td>
        </tr>
        <tr>
            <td>lastName</td>
            <td>${lastName}</td>
        </tr>
        <tr>
            <td>email</td>
            <td>${email}</td>
        </tr>
    </table>
</body>

</html>