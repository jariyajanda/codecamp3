package web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import web.domain.Employee;


@Controller
public class LoginFrom {
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView employeeView() {
        return new ModelAndView("login", "command", new Employee());

    }

    @RequestMapping(value = "/registerFrom", method = RequestMethod.POST)
    public String addEmployee(@ModelAttribute("SpringWeb") Employee emp, ModelMap model) {
     
        System.out.println("loginfrom get parameter");
     
            model.addAttribute("email", emp.getEmail());
            model.addAttribute("firstName", emp.getFirstName());
            model.addAttribute("lastName", emp.getLastName());
            return "hello";
        }
    }

