package web.exception;

public class ExceptionError extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ExceptionError(String message) {
        super(message);
    }

}