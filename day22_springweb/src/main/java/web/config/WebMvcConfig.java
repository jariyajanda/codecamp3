package web.config;

import java.util.Properties;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import web.exception.ExceptionError;
import web.exception.SpringException;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "web" })
public class WebMvcConfig implements WebMvcConfigurer {
    @Bean
    public ViewResolver beanNameResolver() {
        return new BeanNameViewResolver();
    }

    @Bean
    public InternalResourceViewResolver resolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public HandlerExceptionResolver errorHandler() {
        SimpleMappingExceptionResolver s = new SimpleMappingExceptionResolver();
        Properties p = new Properties();
        p.setProperty(SpringException.class.getName(), "springExceptionView");
        s.setExceptionMappings(p);
        s.setDefaultStatusCode(400);
        return s;
    }

    @Bean
    public HandlerExceptionResolver errorHandler2() {
        SimpleMappingExceptionResolver ss = new SimpleMappingExceptionResolver();
        Properties p = new Properties();
        p.setProperty(ExceptionError.class.getName(), "login");
        ss.setExceptionMappings(p);
        ss.setDefaultStatusCode(400);
        return ss;
    }

    // การสร้าง view โดยไม่ใช้ jsp
    @Bean("jsonView")
    public View jsonView() {
        MappingJackson2JsonView view = new MappingJackson2JsonView();
        view.setPrettyPrint(true);
        return view;
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("ValidationMessages");
        return messageSource;
    }
}
