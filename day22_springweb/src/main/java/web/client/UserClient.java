package web.client;

import org.springframework.web.client.RestTemplate;

public class UserClient {
    public static void main(String[] args) {
        String uri = "https://jsonplaceholder.typicode.com/todos/1";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);
        System.out.println(result);

    }
}