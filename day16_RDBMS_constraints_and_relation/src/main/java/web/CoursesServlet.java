package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class CoursesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp", "root",
                    "password");
            Statement statement = connection.createStatement();
            // String strsql = "select instructors.name from instructors left join courses
            // on instructors.id = courses.teach_by where teach_by is null order by
            // instructors.id";
            String strsql = "select courses.name from courses left join instructors on instructors.id= courses.teach_by where teach_by is null order by instructors.id";
            ResultSet resultSet = statement.executeQuery(strsql);

            ArrayList<Instructors> Ins = new ArrayList<Instructors>();
            while (resultSet.next()) {

                Instructors inst = new Instructors();
                String strname = resultSet.getString("name");
                inst.setName(strname);
                Ins.add(inst);
            }
            statement.close();
            connection.close();
            req.setAttribute("Ins", Ins);
            req.getRequestDispatcher("/jsp/courses.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
