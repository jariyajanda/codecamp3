package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class TeacherServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp", "root",
                    "password");
            Statement statement = connection.createStatement();
            String strsql = "select instructors.name from instructors left join courses on instructors.id = courses.teach_by  where teach_by is null order by instructors.id";
            ResultSet resultSet = statement.executeQuery(strsql);

            ArrayList<Courses> cours = new ArrayList<Courses>();
            while (resultSet.next()) {

                Courses cos = new Courses();
                String strname = resultSet.getString("instructors.name");

                cos.setName(strname);

                cours.add(cos);
            }
            connection.close();
            req.setAttribute("cours", cours);
            req.getRequestDispatcher("/jsp/teacherUn.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
