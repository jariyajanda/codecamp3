package web;

public class Courses {
    private String Cousid;
    private String name;
    private String detail;
    private int price;
    private String teach_by;

    /**
     * @return the cousid
     */
    public String getCousid() {
        return Cousid;
    }

    /**
     * @param cousid the cousid to set
     */
    public void setCousid(String cousid) {
        Cousid = cousid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the teach_by
     */
    public String getTeach_by() {
        return teach_by;
    }

    /**
     * @param teach_by the teach_by to set
     */
    public void setTeach_by(String teach_by) {
        this.teach_by = teach_by;
    }

}
