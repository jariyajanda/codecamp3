package web;

public class Instructors {
    private String Instid;
    private String name;
    private String datetime;

    /**
     * @return the instid
     */
    public String getInstid() {
        return Instid;
    }

    /**
     * @param instid the instid to set
     */
    public void setInstid(String instid) {
        Instid = instid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}