show databases;

select courses.id, courses.name, instructors.name
from courses
    left join instructors on instructors.id = courses.teach_by
order by courses.id;



SELECT *
from instructors;

select courses.id, courses.name as course_name, instructors.name as instructor_name
from courses
    left join instructors on instructors.id = courses.teach_by
order by courses.id;

select c.id, c.name as course_name, i.name as instructor_name
from courses as c
    left join instructors as i on i.id = c.teach_by
order by c.id;


select courses.id, courses.name , instructors.name
from courses
    inner join instructors on instructors.id = courses.teach_by
order by courses.id;

select c.id, c.name as course_name, i.name as instructor_name
from courses as c
    full outer join instructors as i on i.id = c.teach_by
order by c.id;

select c.id, c.name as course_name, i.name as instructor_name
from courses as c
    full outer join instructors as i on i.id = c.teach_by;

select courses.id, courses.name, instructors.name
from courses
    left join instructors on instructors.id = courses.teach_by
order by courses.id;


select instructors.id as idinst, instructors.name, courses.id, courses.name
select instructors.name
from instructors left join courses on instructors.id = courses.teach_by
where teach_by is null
order by instructors.id;

select courses.id , courses.name as cousName, instructors.name as insName
from courses left join instructors on instructors.id
= courses.teach_by
where teach_by
is null
order by instructors.id;



INSERT INTO students
    (name, created_at,course)
VALUES('uuuu', now(), 'Conservation');
alter table students add column course varchar
(255) not null;
select distinct name
from courses;
select *
from courses;
describe students;
Cooking
Acting
Chess
Writing
Conservation
select distinct courses.name, courses.id, students.name
from students
    left join courses on students.course = courses.name
where students.name is not null
order by courses.id;

alter table students add column idcourse int not null;



select *
from courses;
select *
from students;


update students set idcourse = 5
where id = 8;
select price, count(*)
from courses
group by price;

select course.id, count(*)
from enrolls join students on students.id = enrolls.student_id
group by course_id;

select courses.name, count(*)
from courses join students on students.course = courses.id
group by course_id
having count(*) > 1;

select name, price
from courses
where price =(select max(price)
from courses);

select courses.name, count(*)
from students
    inner join courses on courses.id = students.idcourse
group by courses.id;

select name, students
from(
select name, (
select count(*)
        from students
        where students.idcourse = courses.id)
    from courses) as t
where students > 0;

INSERT INTO enrolls
    (student_id,course_id)
VALUES
    ('1', '1'),
    ('1', '2'),
    ('1', '5'),
    ('1', '8'),
    ('2', '3'),
    ('4', '3'),
    ('4', '5');

select *
from students;
desc students;

INSERT INTO students
    (name)
VALUES
    ('Ploy'),
    ('Kwang'),
    ('boy'),
    ('sary'),
    ('sara'),
    ('M'),
    ('Emma'),
    ('Peter'),
    ('John'),
    ('King');

select DISTINCT courses.name
from enrolls
    left join courses on enrolls.course_id = courses.id;

select c.name, c.price
from courses c
    left join enrolls e
    on e.course_id = c.id
where e.student_id is null
order by c.name;

1.1
select sum(courses.price)
from courses
    left join enrolls on courses.id = enrolls.course_id
where courses.id = enrolls.course_id;
1.2
select students.name as studentName, sum(price)
from courses
    inner join enrolls on enrolls.course_id = courses.id
    inner join students on students.id = enrolls.student_id
group by students.name;


select *
from courses
    inner join enrolls on enrolls.course_id = courses.id;

select enrolls.student_id , courses.name as coursesName, sum(price) as sumPrice
from courses inner join enrolls on enrolls.course_id = courses.id;

select sum(courses.price) as sum, count(courses.id)
from courses
    left join enrolls on courses.id = enrolls.course_id
where courses.id = enrolls.course_id

select students.name as studentName, sum(price) as sumPrice, count(courses.id)
from courses inner join enrolls on enrolls.course_id = courses.id
    inner join students on students.id = enrolls.student_id
group by students.name;

select students.id, students.name, ANY_VALUE(courses.id) as courses_id, ANY_VALUE(courses) as courses_name, avg(courses.price) as MAX_courses_price
from students
    INNER join enrolls ON enrolls.student_id = students.id
    INNER join courses ON courses.id = enrolls.course_id
GROUP BY students.id




select t2.id, t2.name , cs.name, cs.id, cs.price
from courses cs
    INNER JOIN
    (select s.id, s.name, max(price) as maxprice
    from students s
        join enrolls e on s.id = e.student_id
        join courses c on e.course_id =c.id
    group by s.id) as t2
    on cs.price = t2.maxprice;
