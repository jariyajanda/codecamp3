package com.codecamp.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.jdbc.Driver;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {

            String username = req.getParameter("username");
            String password = req.getParameter("password");

            // Step Connection DataBase Mysql
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/codecamp3_employee",
                    "root", "password");

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user");

            ArrayList<User> users = new ArrayList<User>();

            String x = "username/password is not correct";
            while (resultSet.next()) {

                User user = new User();
                String studentId = resultSet.getString("id");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                Integer salary = resultSet.getInt("salary");
                String usernameDB = resultSet.getString("username");
                String passwordDB = resultSet.getString("password");
                String company = resultSet.getString("company");

                user.setStudentid(studentId);
                user.setFirstname(firstname);
                user.setLastname(lastname);
                user.setSalary(salary);
                user.setUsername(usernameDB);
                user.setPassword(passwordDB);
                user.setCompany(company);
                users.add(user);

                if (username.equals(usernameDB) && password.equals(passwordDB)) {
                    HttpSession session = req.getSession();
                    session.setMaxInactiveInterval(60);
                    session.setAttribute("FirstName", firstname);
                    session.setAttribute("LastName", lastname);
                    session.setMaxInactiveInterval(30);

                    System.out.println("firstname: " + firstname);
                    System.out.println("lastname: " + lastname);
                    System.out.println("session firstname : " + session.getAttribute("Firstname"));
                    System.out.println("session lastname : " + session.getAttribute("Lastname"));
                    resp.sendRedirect("member");
                } else {
                    req.setAttribute("result", x);
                    System.out.println("sesion result : " + req.getAttribute("result"));
                    req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);

                }
                // System.out.println(String.format("ID=%s, Firstname=%s, Lastname%s",
                // studentId, firstname, lastname));
            }

            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);

    }

}