<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
</head>

<body>
    <c:if test="${sessionScope.FirstName !=null}">
        <h3>You have signed-in as ${sessionScope.FirstName}${sessionScope.LastName}</h3>
    </c:if>
    <form method="POST" action="login">
        <p>
            <label>Username: </label>
            <input type="text" name="username" />
        </p>
        <p>
            <label>Password: </label>
            <input type="password" name="password" />
        </p>
        <h2>${result}</h2>

        <p>
            <button type="submit">Login</button>
        </p>
    </form>
</body>

</html>