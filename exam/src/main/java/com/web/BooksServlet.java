package com.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class BooksServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop", "root",
                    "password");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM employee");

            ArrayList<Employee> emps = new ArrayList<Employee>();
            while (resultSet.next()) {

                Employee emp = new Employee();
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                Integer age = resultSet.getInt("age");
                emp.setFirstname(firstname);
                emp.setLastname(lastname);
                emp.setAge(age);
                emps.add(emp);

            }
            System.out.println(emps);
            statement.close();
            connection.close();
            req.setAttribute("emps", emps);
            req.getRequestDispatcher("/jsp/books.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
