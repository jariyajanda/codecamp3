package com.web;

public class job {
    private String id;
    private String job_name;

    private int saraly;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the job_name
     */
    public String getJob_name() {
        return job_name;
    }

    /**
     * @param job_name the job_name to set
     */
    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    /**
     * @return the saraly
     */
    public int getSaraly() {
        return saraly;
    }

    /**
     * @param saraly the saraly to set
     */
    public void setSaraly(int saraly) {
        this.saraly = saraly;
    }

}
