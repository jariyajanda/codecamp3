package com.web;

public class BookShop {
    private String ISBN;
    private String namebook;
    private int price;

    /**
     * @return the iSBN
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * @param iSBN the iSBN to set
     */
    public void setISBN(String iSBN) {
        ISBN = iSBN;
    }

    /**
     * @return the namebook
     */
    public String getNamebook() {
        return namebook;
    }

    /**
     * @param namebook the namebook to set
     */
    public void setNamebook(String namebook) {
        this.namebook = namebook;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

}
