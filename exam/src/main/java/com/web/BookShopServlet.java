package com.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class BookShopServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop", "root",
                    "password");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM book");

            ArrayList<BookShop> bookshop = new ArrayList<BookShop>();
            while (resultSet.next()) {
                BookShop bs = new BookShop();
                String isbn = resultSet.getString("ISBN");
                String namebook = resultSet.getString("namebook");
                Integer price = resultSet.getInt("price");
             
                bs.setISBN(isbn);
                bs.setNamebook(namebook);
                bs.setPrice(price);
                bookshop.add(bs);
            }

            statement.close();
            connection.close();
            req.setAttribute("bookshop", bookshop);
            req.getRequestDispatcher("/jsp/bookshop.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
