<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
</head>

<body>
    <table border="1">
        <tr>

            <th>ISBN</th>
            <th>Book Name</th>
            <th>Price</th>
        </tr>
        <c:forEach var="item" items="${bookshop}">
            <tr>
                <td>
                    <c:out value="${item.getISBN()}" />
                </td>
                <td>
                    <c:out value="${item.getNamebook()}" />
                </td>
                <td>
                    <c:out value="${item.getPrice()}" />
                </td>
            </tr>
        </c:forEach>
    </table>
</body>

</html>