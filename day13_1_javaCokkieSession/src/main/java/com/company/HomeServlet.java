package com.company;

import java.io.IOException;
import java.util.Optional;
import java.time.LocalDateTime;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Optional<Cookie> cookieOptional = Optional.empty();
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : req.getCookies()) {
                if (cookie.getName().equals("username1")) {
                    cookieOptional = Optional.of(cookie);
                    break;
                }
            }
        }
        String time = LocalDateTime.now().toString();

        Cookie usernameCookie;
        if (!cookieOptional.isPresent()) {
            usernameCookie = new Cookie("username1", time);
            usernameCookie.setMaxAge(600);
            resp.addCookie(usernameCookie);
        }
        req.getRequestDispatcher("/jsp/home.jsp").forward(req, resp);

    }
}
