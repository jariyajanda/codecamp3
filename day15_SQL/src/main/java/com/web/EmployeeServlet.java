package com.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.jdbc.Driver;

public class EmployeeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/exam", "root", "password");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select employee.id,employee.first_name,employee.last_name,job.job_name from employee left join job  on employee.job_id = job.id");

            ArrayList<Employee> employee = new ArrayList<Employee>();
            ArrayList<Job> jobarr = new ArrayList<Job>();
            while (resultSet.next()) {
                Employee emp = new Employee();
                String id = resultSet.getString("id");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                String jobname = resultSet.getString("job.job_name");
                Job job = new Job();

                emp.setId(id);
                emp.setFirstname(firstname);
                emp.setLastname(lastname);
                job.setJob_name(jobname);
                employee.add(emp);

            }
            System.out.println(employee);
            statement.close();
            connection.close();
            req.setAttribute("employee", employee);
            // req.setAttribute("jobarr", jobarr);
            req.getRequestDispatcher("/jsp/employeedis.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
