<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
</head>

<body>
    <table border="1">
        <tr>

            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
        </tr>
        <c:forEach var="item" items="${emps}">
            <tr>
                <td>
                    <c:out value="${item.getFirstname()}" />
                </td>
                <td>
                    <c:out value="${item.getLastname()}" />
                </td>
                <td>
                    <c:out value="${item.getAge()}" />
                </td>
            </tr>
        </c:forEach>
    </table>

</body>

</html>